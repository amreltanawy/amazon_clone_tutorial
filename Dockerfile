FROM node:latest

RUN mkdir /app
WORKDIR /app

RUN npm install -g nodemon
RUN npm install --save
EXPOSE 3000

CMD ["nodemon"]
