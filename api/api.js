let router      = require('express').Router();
let Async       = require('async');
let faker       = require('faker');
let Category    = require('../models/category');
let Product     = require('../models/product');


router.post('/search', function(req,res,next){
    Product.search({
        query_string: { query: req.body.search_form}
    }, function(err,results){
        if(err) return next(err);
        res.json(results);
    });
});


router.get('/:name', function(req,res,next){
    Async.waterfall([
        function(callback){
            Category.findOne({name: req.params.name}, function(err,cat){
                if(err) return next(err);
                callback(null,cat);
            })

        },
        function(cat,callback){
            for ( var i = 0; i<30; i++){
                let product         = new Product();
                product.category    = cat._id;
                product.name        = faker.commerce.productName();
                product.price       = faker.commerce.price();
                product.image       = faker.image.image();

                product.save();
            }

        }
    ]);
    res.json({message: 'Success'});
});

module.exports = router;
