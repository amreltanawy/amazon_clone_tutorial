exports = {
    LOGIN: passport.authenticate('local-login',{
        successRedirect: '/profile',
        failureRedirect: '/login', 
        failureFlash: true
    }),
    LOGOUT: (req,res)=>{
        req.logout(); // .logout() is a method added to the request object by the authentication middlewares
        res.redirect('/');
    }
}