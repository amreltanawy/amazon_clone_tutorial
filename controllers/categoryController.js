var CategoryModel = require('../models/category');
module.exports = {
    CREATE: (req, res, next) => {
        try {
            let tempCategory = new CategoryModel();

            tempCategory.name = req.body.name;

            tempCategory.save(function (err) {
                if (err) return next(err);
                req.flash('success', 'Successfully added the new category');
                return res.redirect('/add-category');

            })


        } catch (error) {
            res.send("there was an error while creating user: \n" + error);
        }



    },
    LIST: (req, res, next)=>{
        CategoryModel.find({},(err, categories)=>{
            if(err) return next(err);
            res.locals.categories = categories;
            next();

        })
    }
}