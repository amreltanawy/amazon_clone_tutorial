var UserModel = require('../models/user');
let async = require('async');
let Cart = require('../models/cart');
module.exports = {
    CREATE: function (req, res, next) {
        try {
            async.waterfall([function (callback) {

                    var tempUser = new UserModel();

                    console.log(JSON.stringify(req.body));

                    tempUser.profile.name = req.body.name;
                    tempUser.password = req.body.password;
                    tempUser.email = req.body.email;
                    tempUser.profile.picture = tempUser.gravatar();

                    UserModel.findOne({
                        "email": tempUser.email
                    }, (err, userExist) => {
                        if (!userExist)

                        {
                            console.log(userExist);
                            tempUser.save((err) => {
                                if (err) {
                                    return next(err);
                                } else {
                                    // console.log('success from save');
                                    // res.json(tempUser);

                                    // return res.redirect('/');

                                    callback(null, tempUser);

                                }
                            });

                        } else {
                            req.flash('errors', "this email already exists");
                            return res.redirect('/signup');

                            // res.json({
                            //     "error": "email already exists"
                            // });
                        }
                    });

                },
                function (user) {

                    let cart = new Cart();
                    cart.Owner = user._id;
                    cart.save(function (err) {
                        if (err) return next(err);
                        req.logIn(user, function (err) {
                            if (err) return next(err);
                            res.redirect('/profile');

                        });



                    });
                }
            ]);



        } catch (error) {
            res.send("there was an error while creating user: \n" + error);
        }



    },
    READ: (req, res, next) => {
        try {
            UserModel.findOne({
                _id: req.user._id
            }, (err, user) => {
                if (err) return next(err);
                res.render('accounts/profile', {
                    user: user
                });
            });

        } catch (error) {
            console.log("error from /profile" + error);
            res.redirect('/');
        }
    },
    UPDATE: (req, res, next) => {
        UserModel.findById({
            _id: req.user._id
        }, function (err, user) {
            if (err) return next(err);

            if (req.body.name) user.profile.name = req.body.name;
            if (req.body.address) user.address = req.body.address;

            user.save(function (err) {
                if (err) return next(err);

                req.flash('profile-edit-success', 'successfully edited the profile details');
                return res.redirect('/edit-profile');
            });



        });
    }
};