const express = require('express'); // the core framework for webdev in node
const morgan = require('morgan'); // the morgan middleware is used to log every request to the server
var mongoose = require('mongoose'); // mongo ORM (Object Relational Mapper) used to create model (database schema) in a json object notation
var bodyParser = require('body-parser'); //parses the json formated body types doesnt work well with multipart request(file uploads)
var UserRoutes = require('./routes/userRoutes'); // user related routes handler (route file for users)
var MainRoutes = require('./routes/mainRoutes'); // public routes handler (route file for the website)
let AdminRoutes = require('./routes/adminRoutes');
let ApiRoutes = require('./api/api');
const secret = require('./config/secret');
var ejs = require('ejs'); // http template engine
var ejsMate = require('ejs-mate'); // auxilery to ejs
var session = require('express-session'); // server side session handler by itself it saves session in memory(unreliable)
var cookieParser = require('cookie-parser'); // cookie handler for session id storage and retrieval client side
var flash = require('express-flash'); // uses express-session and cookie-parser to validate session
var MongoStore = require('connect-mongo')(session);
var passport = require('passport');
let categoryController = require('./controllers/categoryController');
let CartMiddleware     = require('./middleware/cartMiddleware');

var app = express();



mongoose.connect(secret.DATABASE, (err) => {
    if (err) console.log("error connecting to database");
    else console.log(" connected to database");
});



const port = secret.PORT;

app.use(express.static(__dirname + '/public'));
app.engine('ejs', ejsMate);
app.set('view engine', 'ejs');
app.use(morgan('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(cookieParser());
app.use(session({
    store: new MongoStore({
        url: secret.DATABASE,
        autoReconnect: true
    }),
    resave: true,
    saveUninitialized: true,
    secret: secret.SECRETKEY
}));
app.use(flash());
app.use(passport.initialize());
app.use(passport.session());
app.use((req, res, next) => {
    res.locals.user = req.user;
    next();
});
app.use(CartMiddleware);
app.use((req, res, next) => {
    categoryController.LIST(req, res, next);
});


//start routing
app.use(MainRoutes);
app.use(UserRoutes);
app.use(AdminRoutes);
app.use('/api',ApiRoutes);

app.all('*', (req, res) => {
    res.json({
        error: 404,
        message: `the url ${req.url} does not exist`,
        request: `this is the request body ${JSON.stringify(req.body)}`
    });
});

app.listen(port, (err) => {
    if (err) throw err;
    console.log(`the server is running on port ${port}`);
});