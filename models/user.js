let mongoose    = require('mongoose');
let Schema      = mongoose.Schema;
let bcrypt      = require('bcrypt-nodejs');
let crypto      = require('crypto');


// the user schema attributes

let UserSchema = Schema({
    email : {type: String , unique: true, lowercase: true,required:true},
    password: String,



    profile: {
        name: {type: String, default: ''},
        picture: {type: String, default: ' '}
    },

    address: String,
    history:[{
        date: Date,
        paid: { type: Number, default: 0}
        // item: {type: Schema.Types.ObjectId, ref: ''}
    }]
});

// var user = new UserModel();




// hash password before saving it to database

    


        // UserSchema.pre('save',(next)=>{
        //     let user = this;
        //     bcrypt.hash(user.password,null,null, function(err, hash) {
        //         if(err) return next(err);
        //         user.password = hash;
                
        //         next();
        //       });
        // });
    UserSchema.pre('save', function (done){
        let user = this;
    
        // console.log(!user.isModified('password'));
        if(!user.isModified('password')) return done();
        bcrypt.genSalt(10, (err,salt )=>{
            if(err) return next(err);
            bcrypt.hash(user.password,salt, null,(err, hash)=>{
                console.log(err);
                if(err) return next(err);
                
                user.password = hash;
                // console.log(`this is the new hash: ${hash} \n\n\n`);
                // console.log("\n\n\n\n\n\n\nREACHED HERE REACHED HERE REACHED HERE\n\n\n\n\n");
    
                done();
    
            });

        });

    });
   



// compare password in the database and the one that the user type in

UserSchema.methods.comparePassword = function(password){
    try {
        return bcrypt.compareSync(password,this.password);
        
    } catch (error) {
        console.log(`this is the user password: ${this.password}\n
        this is the input password ${password}\n`+ error);
        
    }
};

UserSchema.methods.gravatar = function(){
    if(!this.size) size = 200;
    if(!this.email) return "https://gravatar.com/avatar/?s=" + size + "&d=retro";
    var md5 = crypto.createHash('md5').update(this.email).digest('hex');
    return `https://gravatar.com/avatar/${md5}?s=${size}&d=retro`;
}

// module.exports = mongoose.model('UserModel',UserSchema,'User');
 

var UserModel = mongoose.model('UserModel',UserSchema);

module.exports = UserModel;
