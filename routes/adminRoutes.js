const router = require('express').Router();
let categoryController = require('../controllers/categoryController');



router.get('/add-category',(req,res)=>{
    res.render('main/add-category', {message: req.flash('success')});
});



router.post('/add-category',(req,res,next)=>{

    categoryController.CREATE(req,res,next);
});







module.exports = router;