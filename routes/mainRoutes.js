const router = require('express').Router();
let Category = require('../models/category');
let Product = require('../models/product');
let Cart = require('../models/cart');

// shoudlnt be here doesnt make sense
// initiating elastic search connection and mapping to index
// the data for easier search

Product.createMapping(function (err, mapping) {
    if (err) {
        console.log('error creating mapping');
        console.log(err);
    } else {
        console.log("mapping created");
        console.log(mapping);
    }
});

let stream = Product.synchronize();
let count = 0;

stream.on('data', function () {
    count++;
});

stream.on('close', function () {
    console.log(`Indexed ${count} documents`);
});

stream.on('error', function (err) {
    console.log(err);
});

// END OF INITIALIZATION

// START OF SEARCH ROUTES

router.post('/search', (req, res, next) => {
    res.redirect(`/search?q=${req.body.q}`);
});

router.get('/search', function (req, res, next) {
    if (req.query.q) {
        Product.search({
            query_string: {
                query: req.query.q
            }
        }, function (err, results) {
            if (err) return next(err);
            let data = results.hits.hits.map((hit) => {
                return hit;
            });
            console.log("data from results is ");
            console.log(data);
            res.render('main/search-result', {
                query: req.query.q,
                data: data
            });
        });
    }
});


// END OF SEARCH ROUTES

// START PAGINATION FUNCTION

function paginate(req, res, next) {
    let perPage = 9;
    let page = req.params.page - 1 || 0;

    Product.find()
        .skip(perPage * page)
        .limit(perPage)
        .populate('category')
        .exec(function (err, products) {
            if (err) return next(err);
            Product.count().exec(function (err, count) {
                if (err) return next(err);
                res.render('main/product-main', {
                    products: products,
                    pages: count / perPage
                });
            });
        });


}

router.get('/page/:page', function (req, res, next) {
    paginate(req, res, next);
});


// END OF PAGINATION FUNCTION

router.get('/products-by-category/:categoryName', function (req, res, next) {
    Category.findOne({
        name: req.params.categoryName
    }, function (err, cat) {
        if (err) return next(err);
        console.log(cat._id);
        Product.find({
                category: cat._id
            })
            .populate('category')
            .exec(function (err, products) {
                if (err) return next(err);
                console.log(products);
                res.render('main/category', {
                    products: products,
                    category: req.params.categoryName
                });
            });
    });
});


router.get('/cart', function (req, res, next) {
    Cart.findOne({
        Owner: req.user._id
    }).populate('items.item').exec(function (err, foundCart) {
        if (err) return next(err);

        res.render('main/cart', {
            foundCart: foundCart,
            message: req.flash('remove')
        });
    });
});

router.get('/product/:id', function (req, res, next) {
    Product.findOne({
            _id: req.params.id
        }).populate('category')
        .exec(function (err, product) {
            if (err) return next(err);
            console.log(product);
            if (!product) {
                return res.json({
                    message: "product does not exist"
                });
            }
            res.render('main/Product', {
                product: product
            });
        });
});

router.post('/product/:product_id', function (req, res, next) {
    Cart.findOne({
        Owner: req.user._id
    }, function (err, cart) {
        if (err) return next(err);
        if (cart) {

            cart.items.push({
                item: req.body.product_id,
                price: parseFloat(req.body.priceValue),
                quantity: parseInt(req.body.quantity)
            });

            cart.total = (cart.total + parseFloat(req.body.priceValue)).toFixed(2);

            cart.save(function (err) {
                if (err) return next(err);
                return res.redirect('/cart');
            });
        }

    });
});

router.post('/remove', function (req, res, next) {
    Cart.findOne({
        Owner: req.user._id
    }, function (err, cart) {
        if (err) return next(err);
        if (cart) {

            cart.items.pull(String(req.body.item));

            cart.total = (cart.total - parseFloat(req.body.price)).toFixed(2);

            cart.save(function (err) {
                if (err) return next(err);
                req.flash('remove','successfully removed');
                res.redirect('/cart');
            });
        }

    });
});

router.get('/', (req, res, next) => {
    if (req.user) {
        paginate(req, res, next);
    } else {

        res.render('main/home');
    }
});








module.exports = router;