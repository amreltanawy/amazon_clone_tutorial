const router = require('express').Router();
var UserModel = require('../models/user');
var passport = require('passport');
var passportConf = require('../config/passport');
var userController = require('../controllers/userController');
// var bodyPareser = require('body-parser');


// login get and post routes to handle login

router.get('/login', (req, res) => {
    if (req.user) return res.redirect('/');
    else
        res.render('accounts/login', {
            message: req.flash('LoginMessage')
        });
});

router.post('/login', passport.authenticate('local-login', {
    successRedirect: '/profile',
    failureRedirect: '/login',
    failureFlash: true
}));


// logout route implementation

router.get('/logout', (req, res) => {
    req.logout(); // .logout() is a method added to the request object by the authentication middlewares
    res.redirect('/');
});


// adding a route for user profile

router.get('/profile', (req, res, next) => {
    userController.READ(req, res, next);
});
// (req, res, next)=>{
//     try {
//         UserModel.findOne({_id: req.user._id}, (err, user)=>{
//             if(err) return next(err);
//             res.render('accounts/profile', {user: user});
//         });

//     } catch (error) {
//         console.log("error from /profile" + error);
//         res.redirect('/');
//     }
// });

// signup get and post routes to handle signup

router.get('/signup', (req, res) => {
    res.render('accounts/signup', {
        errors: req.flash('errors')
    });

});


router.post('/signup', (req, res, next) => {
    userController.CREATE(req, res, next);
});


router.get('/edit-profile', (req, res) => {
    res.render('accounts/edit-profile', {
        message: req.flash('profile-edit-success')
    });
})

router.post('/edit-profile', (req, res, next) => {
    userController.UPDATE(req, res, next);
});

/*

(req, res, next) => {
    try {
        var tempUser = new UserModel();

        console.log(JSON.stringify(req.body));

        tempUser.profile.name = req.body.name;
        tempUser.password = req.body.password;
        tempUser.email = req.body.email;

        UserModel.findOne({
            "email": tempUser.email
        }, (err, userExist) => {
            if (!userExist)

            {
                console.log(userExist);
                tempUser.save((err) => {
                    if (err) {
                        return next(err);
                    } else {
                        // console.log('success from save');
                        // res.json(tempUser);

                        // return res.redirect('/');
                        req.logIn(tempUser, function(err){
                            if (err) return next(err);
                            res.redirect('/profile');
                        })
                    }
                });

            } else {
                req.flash('errors', "this email already exists");
                return res.redirect('/signup');

                // res.json({
                //     "error": "email already exists"
                // });
            }
        });


    } catch (error) {
        res.send("there was an error while creating user: \n" + error);
    }



});
*/

module.exports = router;